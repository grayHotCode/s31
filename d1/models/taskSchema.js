
const mongoose = require('mongoose');

const taskSchema = new mongoose.Schema({

	name: {
		type: String,
		required: [true, 'Task Name is required']
	},
	status: {
		type: String,
		default: "Pending"
	}
});


// My Solutions
// const taskSchema = new mongoose.Schema({

// 	name: String,
// 	status: {
// 		type: String,
// 		default: "Complete"
// 	}
// });


module.exports = mongoose.model('Task', taskSchema);

